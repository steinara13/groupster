package group.groupster;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import group.groupster.Models.Friend;

public class FriendActivity extends GlobalActivity{
    ArrayList<String> usernames;
    ArrayAdapter<String> adapter;
    ListView listView;
    EditText myFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addfriend_screen);

        listView = (ListView)findViewById(R.id.getUsers);
        myFilter = (EditText)findViewById(R.id.friendUsername);

        myUsername = sp.getString("username", "notfound");
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new ItemList());

        getAllUsersExceptFriends();

        myFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // Retrieves all users except your friends
    protected void getAllUsersExceptFriends() {
        JsonArrayRequest jreq = new JsonArrayRequest(APIprefix + "users/" + myUsername + "/exeptfriends" , new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                usernames = new ArrayList<>();
                String myUsername = sp.getString("username", "notfound");
                String name;
                try {
                    JSONArray array = new JSONArray(response.toString());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        name = row.getString("username");
                        if (!name.equals(myUsername)) {
                            usernames.add(name);
                        }
                    }
                    System.out.println("usernames except friends " + usernames);
                    initList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error message : " + error.toString());
            }
        });
        Volley.newRequestQueue(this).add(jreq);
    }

    // Populates list for this activity.
    public void initList() {
        adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.txtitems, usernames);
        listView.setAdapter(adapter);
    }

    // Listener for item clicks in list
    class ItemList implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            ViewGroup vg = (ViewGroup)view;
            TextView tv = (TextView)vg.findViewById(R.id.txtitems);
            Friend f = new Friend();
            f._username = tv.getText().toString();
            db.addFriend(f);


            System.out.println("here is the db list: " + db.getAllFriends());
            //addFriend(tv.getText().toString());
            addFriendAPI(tv.getText().toString());

            Toast.makeText(FriendActivity.this, tv.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}

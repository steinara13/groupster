package group.groupster.Models;


public class Group implements Comparable<Group> {
    public String _groupName;

    public Group() {}

    public Group(String groupName) { this._groupName = groupName; }

    public int compareTo(Group other){
        int cmp = _groupName.compareTo(other._groupName);
        return cmp;
    }
}

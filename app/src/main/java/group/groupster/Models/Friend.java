package group.groupster.Models;


public class Friend implements Comparable<Friend> {
    public String _username;

    public Friend() {}

    public Friend(String username) {
        this._username = username;
    }

    public int compareTo(Friend other){
        int cmp = _username.compareTo(other._username);
        return cmp;
    }

}

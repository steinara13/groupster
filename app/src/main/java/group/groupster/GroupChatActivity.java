package group.groupster;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import org.json.*;


import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class GroupChatActivity extends GlobalActivity {
    ArrayList<String> tmpMessages;
    EditText msg;
    ListView msgList;
    ArrayAdapter<String> adapter;
    String room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groupchat_screen);

        Intent i = getIntent();
        if (i.hasExtra("room")){
            room = i.getStringExtra("room");
        }
        else {
            room = "";
        }
        msg = (EditText) findViewById(R.id.mymessage);
        msgList = (ListView) findViewById(R.id.groupmessages);
        tmpMessages = new ArrayList<>();

        /*tmpMessages.add("Steinar: Heyho");
        tmpMessages.add("Krstinn: mammapabbi");

        adapter = new ArrayAdapter<String>(this, R.layout.message_item, tmpMessages);
        msgList.setAdapter(adapter);*/

    }

    public void showNotification(String user, String message) {
        Intent intent = new Intent(this, GroupChatActivity.class);
        intent.putExtra("room", room);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
        Resources r = getResources();
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("Sup")
                .setSmallIcon(android.R.drawable.star_big_on)
                .setContentTitle("New Message from " + user)
                .setContentText("\"" + message + "\"")
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    public void sendMessage(View view){;
        String input = msg.getText().toString();
        if(input.isEmpty()){
            return;
        }
        msg.setText("");
        System.out.println("Message: " + input);
        mSocket.emit("sendMsg", input, room);

    }

    //Socket listener
    {
       System.out.println("HEHEHE TESTIN");
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                mSocket.emit("foo", "hi");
            }
        }).on("gotMessage", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                System.out.println("Vibrator is " + vibrator);
                if (vibrator.equals(true)) {
                    System.out.println("VIBREATEING BZZZZZZ");
                    vibrate.vibrate(300);
                }
                System.out.println("GOT MESSAGE GROUPCHAT ACTIVITY");
                System.out.println(args[0]);
                String username = "";
                String message = "";
                try {
                    System.out.println("args size = " + args.length);
                    JSONObject JSONroom = new JSONObject(args[1].toString());
                    String toRoom = JSONroom.getString("room");
                    if (!room.equals(toRoom)) {
                        return;
                    }
                    JSONObject data = new JSONObject(args[0].toString());
                    username = data.getString("username");
                    message = data.getString("message");
                    if (!sp.getString("username", "notfound").equals(username)) {
                        showNotification(username, message);
                    }
                } catch (JSONException e) {
                    final CharSequence text = "Error Parsing Message";
                    final Context ctx = getApplicationContext();
                    final int duration = Toast.LENGTH_SHORT;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast toast = Toast.makeText(ctx, text, duration);
                            toast.show();
                        }
                    });
                }
                updateListview(username, message);

            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {}

        });
    }

    public void updateListview(String username, String message){
        tmpMessages.add(0, username + ": " + message);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Updating listview");
                //stuff that updates ui
                adapter = new ArrayAdapter<>(getBaseContext(), R.layout.message_item, tmpMessages);
                msgList.setAdapter(adapter);
            }
        });
    }


}

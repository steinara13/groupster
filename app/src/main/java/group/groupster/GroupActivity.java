package group.groupster;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import group.groupster.Models.Friend;
import group.groupster.Models.Group;

public class GroupActivity extends GlobalActivity {

    ArrayList<String> usernames;
    ArrayList<String> groupMembers;
    ArrayAdapter<String> adapter;
    ArrayList<Friend> friends;
    ListView listView;
    EditText myFilter;
    EditText groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creategroup_screen);

        listView = (ListView)findViewById(R.id.listview);
        myFilter = (EditText)findViewById(R.id.txtsearch);
        groupName = (EditText)findViewById(R.id.groupname);
        groupMembers = new ArrayList<>();

        //getAllUsers();
        usernames = new ArrayList<>();
        friends = new ArrayList<>();
        friends = db.getAllFriends();


        for (int i = 0; i < friends.size(); i++) {
            usernames.add(friends.get(i)._username.toString());
        }
        initList();


        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new ItemList());    //listens for click in userlist

        myFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    // Gets response from API of all users in system.
    protected void getAllUsers() {
        JsonArrayRequest jreq = new JsonArrayRequest(APIprefix + "users", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                String myUsername = sp.getString("username", "notfound");
                String name;
                usernames = new ArrayList<>();
                try {
                    JSONArray array = new JSONArray(response.toString());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        name = row.getString("username");
                        if (!name.equals(myUsername)) {
                            usernames.add(name);
                        }
                    }
                    initList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error message : " + error.toString());
            }
        });
        Volley.newRequestQueue(this).add(jreq);
    }

    // Populates users in Listview.
    public void initList() {
        adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.txtitems, usernames);
        listView.setAdapter(adapter);
    }

    // monitors clicks in group layout layout
    public void groupButtons(View view) {
        if (view.getId() == R.id.createGroup) {
            String name = groupName.getText().toString();

            if (name.isEmpty()) {
                Toast.makeText(GroupActivity.this, "Name cannot be empty" , Toast.LENGTH_SHORT).show();
            } else if (groupMembers.isEmpty()) {
                Toast.makeText(GroupActivity.this, "Group must have other members than you" , Toast.LENGTH_SHORT).show();
            }

            //else add group
            Group g = new Group();
            g._groupName = name;
            final String gName = g._groupName;
            // TODO .. add members to group

            addGroupAPI(g._groupName);

            Runnable runnable = new Runnable() {

                public void run() {

                    ///api/groups/:groupName/users

                    for(int i = 0; i < groupMembers.size(); i++){


                        JSONObject myobj = new JSONObject();
                        try {
                            myobj.put("username", groupMembers.get(i));
                        } catch (JSONException e) {
                            System.out.println("Error: failed to create json object");
                            e.printStackTrace();
                        }

                        JsonObjectRequest req = new JsonObjectRequest(APIprefix + "groups/" + gName + "/users", myobj,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        // hérna þarf að skipta um mynd
                                        System.out.println("Friend added to group !!! " );
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        System.out.println("Error adding friend to group :  " +  error.getMessage());
                                    }
                                });
                        Volley.newRequestQueue(getBaseContext()).add(req);

                    }
                }
            };
            Thread myThread = new Thread(runnable);
            myThread.start();

            System.out.println("Group object: " + g);
            System.out.println("Group name: " + g._groupName);
            System.out.println("Group members: " + groupMembers);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.cancelGroup) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    //groupMembers array
    public void addUserToList(String person) {

        if (!groupMembers.contains(person)) {
            groupMembers.add(person);
        }
    }


    class ItemList implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            ViewGroup vg = (ViewGroup)view;
            TextView tv = (TextView)vg.findViewById(R.id.txtitems);
            String person = tv.getText().toString();

            addUserToList(person);
            Toast.makeText(GroupActivity.this, tv.getText().toString(), Toast.LENGTH_SHORT).show(); // here should we add the user to group...
        }
    }
}
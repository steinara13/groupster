package group.groupster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import group.groupster.Models.Friend;
import group.groupster.Models.Group;
import group.groupster.Models.User;
import io.socket.client.IO;

public class MainActivity extends GlobalActivity {
    LinearLayout inHorizontalScrollView;
    LinearLayout inHorizontalScrollViewGroups;
    ArrayList<String> listOfFriends;

    //Ser þradur fyrir Friends
    class taskGetFriends extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            //Thetta keyrir fyrst
            getAllFriendsAPI();

            //Nota sleep til að bida eftir API-inum
            System.out.println("Wait for a few seconds?");
            try { Thread.sleep( 1000 ); }
            catch ( Exception e ) {}
            System.out.println("Did the waiting happen? who knows....");

            //Þarf að returna null, don't know why
            return null;
        }

        //Keyrt beint a eftir doInBackround sjalfkrafa
        @Override
        protected void onPostExecute(Void aVoid) {
            //super.onPostExecute(aVoid);
            //Thetta er bara console log
            ArrayList<User> allUsers = db.getAllUsers();
            ArrayList<Friend> allFriends = db.getAllFriends();
            System.out.println("Number of users in system: " + allUsers.size());
            System.out.println("Number of friends in system: " + allFriends.size());

            //Thetta er functionid til ad populata friends view
            addUserHorizontal(inHorizontalScrollView);
        }
    }

    // Thread for groups
    class taskGetAllMyGroups extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            getAllMyGroups();

            System.out.println("Wait for a few seconds?");
            try { Thread.sleep( 1000 ); }
            catch ( Exception e ) {}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayList<Group> allmyGroups = db.getAllGroups();
            System.out.println("Number of groups in my system: " + allmyGroups.size());
            addGroupsHorizontal(inHorizontalScrollViewGroups);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inHorizontalScrollView = (LinearLayout)findViewById(R.id.inhorizontalscrollview);
        inHorizontalScrollViewGroups = (LinearLayout)findViewById(R.id.inhorizontalscrollviewgroups);
        myUsername = sp.getString("username", "notfound");


        {
            try {
                System.out.println("trying to connect socket to server");
                mSocket = IO.socket(SocketURL);
                System.out.println("msocket is : " + mSocket.toString());
                mSocket.connect();
                mSocket.emit("setUserName", myUsername);

            } catch (URISyntaxException e) {
                System.out.println("failed to create socket: ");
                System.out.println(e.getMessage());
            }
        }
        new taskGetFriends().execute();
        new taskGetAllMyGroups().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("FRIENDS");
        vibrator = sp.getBoolean("vibrate", false);
        System.out.println("Vibrator : " + vibrator);
        for(Friend f: db.getAllFriends()) {
            System.out.println(f._username);
        }
        displayUser();
    }

    // Populate Linearylayout for groups
    private void addGroupsHorizontal(LinearLayout layout) {
        ArrayList<Group> myGroups = db.getAllGroups();
        Collections.sort(myGroups);


        for (int i = 0; i < myGroups.size(); i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            params.setMargins(10, 10, 10, 10);
            TextView tv = new TextView(this);
            tv.setId(i);
            tv.setClickable(true);
            tv.setText(myGroups.get(i)._groupName.toUpperCase());
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.box));
            tv.setLayoutParams(params);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openGroupChat(view);
                }
            });
            layout.addView(tv);
        }
    }

    // Populate Linearylayout for friends
    private void addUserHorizontal(LinearLayout layout) {

        ArrayList<Friend> myFriends = db.getAllFriends();
        Collections.sort(myFriends);
        for (int i = 0; i < myFriends.size(); i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            params.setMargins(10, 10, 10, 10);
            TextView tv = new TextView(this);
            tv.setText(myFriends.get(i)._username.toUpperCase());
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.box));
            tv.setLayoutParams(params);
            layout.addView(tv);
        }
    }


    // displays the "Welcome User" message
    private void displayUser() {
        TextView userTextView = (TextView) findViewById(R.id.welcomeUser);
        userTextView.setText("Welcome " + myUsername);
    }

    public void logout(View view) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("loggedIn", false);
        editor.commit();
        Intent intent = new Intent(this, LoginActivity.class);
        //Used to clear the activity stack
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        mSocket.disconnect();
    }

    // monitors clicks in activity_main layout
    public void menuClick(View view) {
        if (view.getId() == R.id.prefsButton) {
            Intent intent = new Intent(this, PreferencesActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.addFriends) {
            Intent intent = new Intent(this, FriendActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.addGroup) {
            Intent intent = new Intent(this, GroupActivity.class);
            startActivity(intent);
        }
    }

    public void openGroupChat(View view) {
        TextView txtView = (TextView) view;
        String myRoom = txtView.getText().toString();
        Intent intent = new Intent(this, GroupChatActivity.class);
        intent.putExtra("room", myRoom);
        startActivity(intent);
    }
}

package group.groupster;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import group.groupster.Models.*;

/**
 * Handles all database related operations
 *
 * void addFriend(Friend friend) - Adds a single friend to the DB
 *
 * void populateFriends(ArrayList<String> friends) - Adds all friends to the DB (Empties current one first)
 *
 * ArrayList<Friend> getAllFriends() - Gets a list of all your friends
 *
 * void addGroup(Group group) - Creates a new group
 *
 * void populateGroups(ArrayList) - Adds all groups to the DB (Empties current one first)
 *
 * ArrayList<Group> getAllGroups() - Gets a list of all your groups
 * 
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "GroupsterDB";
    private static final String TABLE_USERS = "Users";
    private static final String TABLE_FRIENDS = "Friends";
    private static final String TABLE_GROUPS = "Groups";
    private static final String TABLE_GROUPMEMBERS = "GroupMembers";

    //Users table
    private static final String USER = "username";

    //Friends table
    private static final String FRIEND = "username";

    //Groups table
    private static final String GROUP = "groupName";

    //Group Members table consist of the above variable (Relational table)


    public DatabaseHandler(Context context) { super(context, DB_NAME, null, DB_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_USERS =         "CREATE TABLE " + TABLE_USERS + "(" +
                                            USER + " TEXT)";
        String CREATE_TABLE_FRIENDS =       "CREATE TABLE " + TABLE_FRIENDS + "(" +
                                            FRIEND + " TEXT)";
        String CREATE_TABLE_GROUPS =        "CREATE TABLE " + TABLE_GROUPS + "(" +
                                            GROUP + " TEXT)";
        String CREATE_TABLE_GROUPMEMBERS =  "CREATE TABLE " + TABLE_GROUPMEMBERS + "(" +
                                            GROUP + " TEXT, " +
                                            FRIEND + " TEXT)";

        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_FRIENDS);
        db.execSQL(CREATE_TABLE_GROUPS);
        db.execSQL(CREATE_TABLE_GROUPMEMBERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FRIENDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPMEMBERS);

        // Create tables again
        onCreate(db);
    }

    //region User table functions

    public void populateUsers(ArrayList<String> user) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Clear the db first (after all we're getting the complete list again)
        db.execSQL("DELETE FROM " + TABLE_USERS);

        ContentValues values;
        //Now we can populate it again
        for(String username: user){
            values = new ContentValues();
            values.put(USER, username);
            db.insert(TABLE_USERS, null, values);
        }

        db.close();
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                User user = new User();
                user._username = cursor.getString(0);
                users.add(user);
            } while(cursor.moveToNext());
        }

        return users;
    }

    //endregion - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //region Friend table functions

    //Adding a single friend
    public void addFriend(Friend friend) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FRIEND, friend._username);

        db.insert(TABLE_FRIENDS, null, values);
        db.close();
    }

    //Called when getting all users from the api to populate the database
    public void populateFriends(ArrayList<String> friend) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Clear the db first (after all we're getting the complete list again)
        db.execSQL("DELETE FROM " + TABLE_FRIENDS);

        ContentValues values;
        //Now we can populate it again
        for(String username: friend) {
            values = new ContentValues();
            values.put(FRIEND, username);
            db.insert(TABLE_FRIENDS, null, values);
        }

        db.close();
    }

    //Gets a list of all your friends
    public ArrayList<Friend> getAllFriends() {
        ArrayList<Friend> friends = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_FRIENDS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Friend friend = new Friend();
                friend._username = cursor.getString(0);
                friends.add(friend);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return friends;
    }

    //endregion - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //region Group table functions

    //Creating a new group
    public void addGroup(Group group) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GROUP, group._groupName);

        db.insert(TABLE_GROUPS, null, values);
        db.close();
    }

    //Called when getting your groups from the api to populate the database
    public void populateGroups(ArrayList<String> group) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Clear the db first (after all we're getting the complete list again)
        db.execSQL("DELETE FROM " + TABLE_GROUPS);

        ContentValues values;
        //now we can populate again
        for(String groupName: group) {
            values = new ContentValues();
            values.put(GROUP, groupName);
            db.insert(TABLE_GROUPS, null, values);
        }

        db.close();
    }

    //Gets a list of all your groups
    public ArrayList<Group> getAllGroups() {
        ArrayList<Group> groups = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_GROUPS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Group group = new Group();
                group._groupName = cursor.getString(0);
                groups.add(group);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return groups;
    }

    //endregion - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
}

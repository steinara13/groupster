package group.groupster;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import group.groupster.Models.Friend;
import io.socket.client.Socket;


public class GlobalActivity extends Activity {
    SharedPreferences sp;
    DatabaseHandler db;
    String myUsername;
    final String APIprefix = "https://grumpster.herokuapp.com/api/";
    final String SocketURL = "https://grumpster.herokuapp.com/";
    static Socket mSocket;
    static Vibrator vibrate;
    static Boolean vibrator = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrate = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        db = new DatabaseHandler(this);
    }

    public void login(View view, String uName) {
        System.out.println("GLOBAL: Login function()");

        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("loggedIn", true);
        editor.putString("username", uName);
        editor.commit();



        Intent intent = new Intent(this, MainActivity.class);

        //Used to clear the activity stack
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // GET - get all users from API and pushes them into our SQL DB
    protected void getAllUsersAPI() {
        System.out.println("API: getAllUsers()");
        JsonArrayRequest jreq=new JsonArrayRequest(APIprefix + "users", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                ArrayList<String> usernames = new ArrayList<>();
                String myUsername = sp.getString("username", "notfound");
                String name;
                try {
                    JSONArray array = new JSONArray(response.toString());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        name = row.getString("username");

                        if (!name.equals(myUsername)) {
                            usernames.add(name);
                        }
                    }

                    db.populateUsers(usernames);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error message : " + error.toString());
            }
        });
        Volley.newRequestQueue(this).add(jreq);
    }

    // GET - get all users from API and pushes them into our SQL DB
    protected void getAllFriendsAPI() {
        //app.get('/api/users/:username/friends'
        System.out.println("API: getAllFriends()");
        String getURI = APIprefix + "users/" + myUsername + "/friends";
        System.out.println(getURI);

        JsonArrayRequest jreq=new JsonArrayRequest(APIprefix + "users/" + myUsername + "/friends", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                ArrayList<String> usernames = new ArrayList<>();
                String name;
                try {
                    JSONArray array = new JSONArray(response.toString());
                    System.out.println("List from API size: " + array.length());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        name = row.getString("username");
                        usernames.add(name);
                    }

                    System.out.println("Adding friends to DB");
                    db.populateFriends(usernames);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error message : " + error.toString());
            }
        });
        Volley.newRequestQueue(this).add(jreq);
    }



    // POST - Add friends to API.
    protected void addFriendAPI(final String friendToAdd){
        //má ekki breyta "notfound"
        System.out.println("My username " + myUsername);
        System.out.println("Adding " + friendToAdd);
        JSONObject myobj = new JSONObject();
        try {
            myobj.put("friend", friendToAdd);
        } catch (JSONException e) {
            System.out.println("Error: failed to create json object");
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(APIprefix + "users/" + myUsername + "/friends", myobj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // hérna þarf að skipta um mynd
                        db.addFriend(new Friend(friendToAdd));
                        System.out.println("i have just added friend " );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Error adding friend :  " +  error.getMessage());
                    }
                });
        Volley.newRequestQueue(this).add(req);
    }

    // GET - get all user groups from API and pushes them into our SQL DB
    protected void getAllMyGroups() {
        //app.get('/api/groups/:username'
        System.out.println("API: getAllMyGroups()");
        String getURI = APIprefix + "groups/" + myUsername;
        System.out.println(getURI);

        JsonArrayRequest jreq = new JsonArrayRequest(getURI, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<String> groups = new ArrayList<>();
                String name;
                try {
                    JSONArray array = new JSONArray(response.toString());
                    System.out.println("List from API size: " + array.length());
                    System.out.println(array);

                    for(int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        name = row.getString("groupName");
                        groups.add(name);
                    }

                    System.out.println("Adding groups to DB");
                    db.populateGroups(groups);
                }
                catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error message : " + error.toString());
            }
        });
        Volley.newRequestQueue(this).add(jreq);
    }

    // POST - Add group to API.
    protected void addGroupAPI(String groupToAdd){
        String myUsername = sp.getString("username", "notfound");
        System.out.println("username is : " + myUsername);
        System.out.println("group to add: " + groupToAdd);
        JSONObject myobj = new JSONObject();
        try {
            myobj.put("groupName", groupToAdd);
        } catch (JSONException e) {
            System.out.println("Error: failed to create json object");
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(APIprefix + "groups/" + myUsername, myobj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // hérna þarf að skipta um mynd
                        System.out.println("i have just added group " );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Error adding group :  " +  error.getMessage());
                    }
                });
        Volley.newRequestQueue(this).add(req);
    }

}
